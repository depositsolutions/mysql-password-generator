# mysql-password-generator

This is a simple Python Flask application that is used to issue MySQL password
hashes according to the validation rules we are trying to enforce.

## Dependencies

* Python 3 is required to run the application
* A MySQL database is required to create the password hash
* All other dependencies are listed inside `dependencies.txt`

## Configuration

The configuration is performed through the following environment variables:

* APP_SECRET_KEY: A random string value that is used by Flask to encrypt the
  session information. The longer the string, the better it is.
* APP_FULL_URL: The full URL under which the application will be served.
* APP_DATABASE_NAME: MySQL database name
* APP_DATABASE_USER: MySQL database user
* APP_DATABASE_PASSWORD: MySQL database password
* APP_DATABASE_HOST: MySQL database host

