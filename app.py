#!/usr/bin/env python3

import mysql.connector
import re
import os
import sys

from flask import Flask, flash, redirect, request, render_template, session, url_for
from mysql.connector import errorcode
from wtforms import Form, BooleanField, StringField, PasswordField, validators


config = {
  'APP_SECRET_KEY': os.environ.get('APP_SECRET_KEY', None),
  'APP_FULL_URL': os.environ.get('APP_FULL_URL', None),
  'APP_DATABASE_NAME': os.environ.get('APP_DATABASE_NAME', None),
  'APP_DATABASE_USER': os.environ.get('APP_DATABASE_USER', None),
  'APP_DATABASE_PASSWORD': os.environ.get('APP_DATABASE_PASSWORD', None),
  'APP_DATABASE_HOST': os.environ.get('APP_DATABASE_HOST', None)
 }


for key in config.keys():
    if config[key] == None:
        print('%s needs to be setup' % key)
        sys.exit(1)

app = Flask(__name__)
app.secret_key = config['APP_SECRET_KEY']
special_chars = '!@#$%&*()'
re_lower = re.compile(r'.*[a-z].*')
re_upper = re.compile(r'.*[A-Z].*')
re_digit = re.compile(r'.*[0-9].*')
re_special = re.compile('.*[%s].*' % re.escape(special_chars))

class PasswordForm(Form):
    password = PasswordField('New Password', [validators.DataRequired(),
                                              validators.EqualTo('confirm', message='Passwords must match'),
                                              validators.length(min=8, message='Password needs at least 8 characters'),
                                              validators.regexp(re_lower, message='Password needs at least one lowercase character'),
                                              validators.regexp(re_upper, message='Password needs at least one uppercase character'),
                                              validators.regexp(re_digit, message='Password needs at least one digit'),
                                              validators.regexp(re_special, message='Password needs at least one of the special chars %s' % special_chars),

                                             ])
    confirm = PasswordField('Repeat Password')

def encode_hash(cleartext):
    try:
        cnx = mysql.connector.connect(user=config['APP_DATABASE_USER'],
                                      password=config['APP_DATABASE_PASSWORD'],
                                      host=config['APP_DATABASE_HOST'],
                                      database=config['APP_DATABASE_NAME'])
        cursor = cnx.cursor(prepared=True)
        query = ("SELECT password(%s)")
        cursor.execute(query, (cleartext,))
        password = 'NO HASH GENERATED'
        for row in cursor:
            password = row[0].decode()
        cursor.close()
        cnx.commit()
        return password
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    finally:
        cnx.close()


@app.route('/', methods=['GET', 'POST'])
def index():
    form = PasswordForm(request.form)
    if request.method == 'POST' and form.validate():
        new_hash = encode_hash(form.password.data)
        flash('New password hash: %s' % new_hash)
        #return redirect(url_for('index', _external=True))
        # The url_for method is not returning the full path of the proxied app,
        # so considering it is already very late and the functionality is in
        # place, let's fix this with more care in the near future.
        return redirect(config['APP_FULL_URL'])
    return render_template('index.html', form=form)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=9000)
